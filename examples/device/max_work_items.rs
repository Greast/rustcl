use rustcl::prelude::*;

fn main(){
    let dev = Platform::all().unwrap().get(0).unwrap().devices(Type::All).pop().unwrap();
    dev.max_work_items();
}