use rustcl::prelude::*;
use rustcl::context::create::Context;

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let devs = &[device];

    let context = devs.context(|_, _| {}).unwrap();

    let _ = context.queue(&devs[0]);
}

