use rustcl::prelude::*;
use rustcl::context::create::Context;
use futures::executor::block_on;
use std::ffi::CString;
use rustcl::memory::Mem;
use cl3::types::cl_int;
use rustcl::queue::Dim;
use core::num::NonZeroUsize;


#[derive(Kernel)]
struct Args<'a>(
    usize,
    Mem<&'a mut [cl_int]>
);

const SRC : &str = "
__kernel void example(size_t size, __global int * input){
    const size_t
        id = get_local_id(0) + get_local_size(0) * get_global_id(0),
        stride = get_local_size(0) * get_global_size(0);
    
    for(size_t i = id ; i < size ; i += stride){
        input[i] = id;
    }
}
";

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let devs = &[device];

    let context = devs.context(|_, _| {}).unwrap();

    let program = context.source(&[SRC]).unwrap();

    let opt = Default::default();
    
    let future = program.compile(devs, &opt, &[], &[]).unwrap();

    let program = block_on(future).unwrap().cast(devs).unwrap();

    let opt = Default::default();

    let future = context.link(devs, &opt, &[program]).unwrap();

    let exec : Executable = block_on(future).unwrap().cast(devs).unwrap();

    let string = CString::new("example").unwrap();

    let mut data = [0 as cl_int];

    let len = data.len();
    
    let mem = context.mem(data.as_mut()).unwrap();
    
    let args = Args(len, mem);

    let mut kernel = exec.kernel(&string, args).unwrap();

    let que = context.queue(&devs[0]).unwrap();

    let dim = Dim{
        offset : 0,
        global : NonZeroUsize::new(1).unwrap(),
        local : NonZeroUsize::new(1).unwrap(),
    };

    let ret = que.enqueue(&mut kernel, &dim.into(), &[]);
    match ret{
        Err(x) => {dbg!(x);},
        _=> (),
    }
    
}

