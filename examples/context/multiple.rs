use rustcl::prelude::*;
use rustcl::context::create::Context;

const SRC : &str = "__kernel void example(){}";

fn main() {
    let devs = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All);

    let _ = devs.context(|_, _| {}).unwrap();
}

