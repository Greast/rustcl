use rustcl::prelude::*;
use rustcl::context::create::Context;
use futures::executor::block_on;
use std::ffi::CString;
use rustcl::memory::Mem;

#[derive(Kernel)]
struct Args<'a>(Mem<&'a [u8]>);

const SRC : &str = "__kernel void example(__global void * input){}";

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let devs = &[device];

    let context = devs.context(|_, _| {}).unwrap();

    let program = context.source(&[SRC]).unwrap();

    let opt = Default::default();
    
    let future = program.compile(devs, &opt, &[], &[]).unwrap();

    let program = block_on(future).unwrap().cast(devs).unwrap();

    let opt = Default::default();

    let future = context.link(devs, &opt, &[program]).unwrap();

    let exec : Executable = block_on(future).unwrap().cast(devs).unwrap();

    let string = CString::new("example").unwrap();

    let data = [0u8];
    
    let mem = context.mem(data.as_ref()).unwrap();
    
    let args = Args(mem);

    let _ = exec.kernel(&string, args).unwrap();
}

