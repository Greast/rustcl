use rustcl::prelude::*;
use rustcl::context::create::Context;

const SRC : &str = "__kernel void example(){}";

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let context = device.context(|_, _| {}).unwrap();

    context.mem([0u8].as_ref()).unwrap();
}
