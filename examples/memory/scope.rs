use rustcl::prelude::*;
use rustcl::context::create::Context;

const SRC : &str = "__kernel void example(){}";

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let context = device.context(|_, _| {}).unwrap();

    let mut data = [0u8];
    {
        context.mem(data.as_ref()).unwrap();
    }
    data[0] = 1;
    
}
