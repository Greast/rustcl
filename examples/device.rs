use rustcl::prelude::*;

fn main() {
    Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();
}
