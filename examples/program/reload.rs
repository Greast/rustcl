use rustcl::prelude::*;
use rustcl::context::create::Context;
use futures::executor::block_on;

const SRC : &str = "__kernel void example(){}";

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let devs = &[device];

    let context = devs.context(|_, _| {}).unwrap();

    let program = context.source(&[SRC]).unwrap();

    let opt = Default::default();

    let future = program.compile(devs, &opt, &[], &[]).unwrap();

    let object = block_on(future).unwrap();

    let binary = object.source();

    let binary = binary.iter().map(Vec::as_slice).collect::<Vec<_>>();

    let object:Object = context.binary(devs, binary.as_slice()).unwrap();

    let opt = Default::default();

    let future = context.link(devs, &opt, &[object]).unwrap();

    let linked = block_on(future).unwrap();

    let binary = linked.source();

    let binary = binary.iter().map(Vec::as_slice).collect::<Vec<_>>();

    let _:Executable = context.binary(devs, binary.as_slice()).unwrap();
}

