use rustcl::prelude::*;
use rustcl::context::create::Context;
use futures::executor::block_on;


const SRC : &str = "__kernel void example(){}";
const SRC2 : &str = "void example2(){}";

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let devs = &[device];

    let context = devs.context(|_, _| {}).unwrap();

    let opt = Default::default();

    let program = context.source(&[SRC])
        .unwrap()
        .compile(devs, &opt, &[], &[])
        .unwrap();
    
    let program = block_on(program)
        .unwrap()
        .cast(devs)
        .unwrap();

    let program2 = context.source(&[SRC2])
        .unwrap()
        .compile(devs, &opt, &[], &[])
        .unwrap();

    let program2 = block_on(program2)
        .unwrap()
        .cast(devs)
        .unwrap();

    let opt = Default::default();

    let future = context.link(devs, &opt, &[program, program2]).unwrap();

    let _ = block_on(future).unwrap();
}

