use rustcl::prelude::*;
use rustcl::context::create::Context;
use futures::executor::block_on;

const SRC : &str = "__kernel void example(){}";

fn main() {
    let device = Platform::all()
        .unwrap()
        .get(0)
        .unwrap()
        .devices(Type::All)
        .pop()
        .unwrap();

    let devs = &[device];

    let context = devs.context(|_, _| {}).unwrap();

    let program = context.source(&[SRC]).unwrap();

    let opt = Default::default();

    let future = program.compile(devs, &opt, &[], &[]).unwrap();

    let program = block_on(future).unwrap();

    program.source();
}

