pub use cl::*;
pub use derive;

pub mod prelude {
    pub use cl::device::*;
    pub use cl::platform::*;
    pub use cl::program::*;
    pub use derive::*;
}

#[cfg(test)]
mod test {
    #[test]
    fn test() {
        let t = trybuild::TestCases::new();
        t.pass("examples/**/*.rs");
    }
}
