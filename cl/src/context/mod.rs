pub mod create;
use cl3::types::cl_context;
pub use create::*;

pub struct Context(pub cl_context);
