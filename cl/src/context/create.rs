use libc::{c_char, c_void, size_t};

use std::ffi::CStr;

pub trait Context {
    type Error;
    fn context<F>(&self, _: F) -> Result<super::Context, Self::Error>
    where
        F: Fn(&CStr, &[u8]);
}

use cl3::error_codes::{CL_DEVICE_NOT_AVAILABLE, CL_OUT_OF_HOST_MEMORY, CL_OUT_OF_RESOURCES};

use enum_utils::{ReprFrom, TryFromRepr};

#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    NoHostMemory = CL_OUT_OF_HOST_MEMORY,
    NoDeviceMemory = CL_OUT_OF_RESOURCES,
    NoDevice = CL_DEVICE_NOT_AVAILABLE,
}

extern "C" fn pfn_notify<F>(
    errinfo: *const c_char,
    private_info: *const c_void,
    cb: size_t,
    user_data: *mut c_void,
) where
    F: Fn(&CStr, &[u8]),
{
    unsafe {
        let errinfo = CStr::from_ptr(errinfo);
        let func_ptr = user_data as *mut F;
        let func = &mut *func_ptr;
        use std::slice::from_raw_parts;
        let slice = from_raw_parts(private_info as *const u8, cb);

        (func)(errinfo, slice);
    }
}

use crate::device::Device;
impl Context for [Device] {
    type Error = Error;
    fn context<F>(&self, callback: F) -> Result<super::Context, Self::Error>
    where
        F: Fn(&CStr, &[u8]),
    {
        let devs = self.iter().map(|x| x.0).collect::<Vec<_>>();
        let data = Box::into_raw(Box::new(callback));

        use cl3::context::create_context;
        use std::ptr::null;

        create_context(&devs, null(), Some(pfn_notify::<F>), data as *mut _)
            .map(super::Context)
            .map_err(Error::try_from)
            .map_err(Result::unwrap)
    }
}

impl Context for [&Device] {
    type Error = Error;
    fn context<F>(&self, callback: F) -> Result<super::Context, Self::Error>
    where
        F: Fn(&CStr, &[u8]),
    {
        let devs = self.iter().map(|x| x.0).collect::<Vec<_>>();
        let data = Box::into_raw(Box::new(callback));

        use cl3::context::create_context;
        use std::ptr::null;

        create_context(&devs, null(), Some(pfn_notify::<F>), data as *mut _)
            .map(super::Context)
            .map_err(Error::try_from)
            .map_err(Result::unwrap)
    }
}

impl Context for Device {
    type Error = <[Device] as Context>::Error;
    fn context<F>(&self, callback: F) -> Result<super::Context, Self::Error>
    where
        F: Fn(&CStr, &[u8]),
    {
        use std::slice::from_ref;
        <[Device] as Context>::context(from_ref(self), callback)
    }
}
