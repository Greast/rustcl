pub mod create;
pub use create::*;
pub mod event;
pub use event::*;
pub mod work;
pub use work::*;

use cl3::error_codes::*;

use enum_utils::{ReprFrom, TryFromRepr};

#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    Executable = CL_INVALID_PROGRAM_EXECUTABLE,
    Queue = CL_INVALID_COMMAND_QUEUE,
    Args = CL_INVALID_KERNEL_ARGS,
    Dims = CL_INVALID_WORK_DIMENSION,
    WorkSize = CL_INVALID_GLOBAL_WORK_SIZE,
    Offset = CL_INVALID_GLOBAL_OFFSET,
    GroupSize = CL_INVALID_WORK_GROUP_SIZE,
    ItemSize = CL_INVALID_WORK_ITEM_SIZE,
    Alloc = CL_MEM_OBJECT_ALLOCATION_FAILURE,
    Wait = CL_INVALID_EVENT_WAIT_LIST,
    NoHostMemory = CL_OUT_OF_HOST_MEMORY,
}

use cl3::types::cl_command_queue;
use core::marker::PhantomData;
pub struct Queue<'a>(pub cl_command_queue, PhantomData<&'a ()>);

use crate::kernel::Kernel;
use cl3::command_queue::enqueue_nd_range_kernel;
impl Queue<'_> {
    pub fn enqueue<'a, T>(
        &self,
        kernel: &'a mut Kernel<T>,
        work: &Work,
        events: &[Event],
    ) -> Result<Event<'a>, Error> {
        let events_ptr = if events.is_empty() {
            core::ptr::null()
        } else {
            events.as_ptr() as *const _
        };

        enqueue_nd_range_kernel(
            self.0,
            kernel.kernel,
            work.offset.len() as u32,
            work.offset.as_ptr(),
            work.gsize.as_ptr(),
            work.lsize.as_ptr(),
            events.len() as u32,
            events_ptr,
        )
        .map(Event::new)
        .map_err(Error::try_from)
        .map_err(Result::unwrap)
    }
}
