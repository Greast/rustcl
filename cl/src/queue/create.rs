use super::Queue;
use crate::context::Context;
use crate::device::Device;

use cl3::error_codes::*;

use enum_utils::{ReprFrom, TryFromRepr};

#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    NoHostMemory = CL_OUT_OF_HOST_MEMORY,
}

use cl3::command_queue::create_command_queue_with_properties;
impl Context {
    pub fn queue<'a>(&'a self, dev: &Device) -> Result<Queue<'a>, Error> {
        use core::marker::PhantomData;
        use std::ptr::null;
        create_command_queue_with_properties(self.0, dev.0, null())
            .map(|x| Queue(x, PhantomData))
            .map_err(Error::try_from)
            .map_err(Result::unwrap)
    }
}
