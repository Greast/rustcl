use cl3::types::*;
use core::marker::PhantomData;
pub struct Event<'a>(pub(crate) cl_event, PhantomData<&'a ()>);

use derive_more::*;
#[derive(Debug, From)]
pub struct StatusError(cl_int);

use core::ffi::c_void;
use futures::channel::oneshot::*;
extern "C" fn pfn_notify(_: cl_event, status: cl_int, user_data: *mut c_void) {
    unsafe {
        let sender = Box::from_raw(user_data as *mut Sender<Result<(), StatusError>>);
        let msg = if 0 <= status {
            Ok(())
        } else {
            Err(StatusError(status))
        };
        sender.send(msg).ok().unwrap();
    }
}

use cl3::error_codes::*;
use enum_utils::{ReprFrom, TryFromRepr};
#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    Value = CL_INVALID_VALUE,
}

use cl3::event::set_event_callback;
impl Event<'_> {
    pub fn new(event: cl_event) -> Self {
        Self(event, PhantomData)
    }
    pub fn wait(&self, status: cl_int) -> Result<Receiver<Result<(), StatusError>>, Error> {
        let (send, recv) = channel();
        let send = Box::into_raw(Box::new(send));

        if let Err(ret) = set_event_callback(self.0, status, pfn_notify, send as *mut _) {
            let err = Error::try_from(ret).unwrap();
            let _ = unsafe { Box::from_raw(send) };
            Err(err)
        } else {
            Ok(recv)
        }
    }
}
