use core::num::NonZeroUsize;
pub struct Dim {
    pub offset: usize,
    pub global: NonZeroUsize,
    pub local: NonZeroUsize,
}

pub struct Work {
    pub(crate) offset: Vec<usize>,
    pub(crate) gsize: Vec<usize>,
    pub(crate) lsize: Vec<usize>,
}

impl From<Dim> for Work {
    fn from(dim: Dim) -> Self {
        Self {
            offset: vec![dim.offset],
            gsize: vec![dim.global.get()],
            lsize: vec![dim.local.get()],
        }
    }
}

impl Work {
    pub fn push(&mut self, dim: Dim) -> &mut Self {
        self.offset.push(dim.offset);
        self.gsize.push(dim.global.get());
        self.lsize.push(dim.local.get());
        self
    }

    pub fn extend(&mut self, other: Self) -> &mut Self {
        self.offset.extend(other.offset);
        self.gsize.extend(other.gsize);
        self.lsize.extend(other.lsize);
        self
    }
}
