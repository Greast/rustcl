use cl3::types::cl_program;
pub struct Source(pub cl_program);

use cl3::error_codes::CL_OUT_OF_HOST_MEMORY;

use enum_utils::{ReprFrom, TryFromRepr};

#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    NoHostMemory = CL_OUT_OF_HOST_MEMORY,
}

use crate::context::Context;
use cl3::program::create_program_with_source;
impl Context {
    pub fn source(&self, strs: &[&str]) -> Result<Source, Error> {
        create_program_with_source(self.0, strs)
            .map(Source)
            .map_err(Error::try_from)
            .map_err(Result::unwrap)
    }
}
