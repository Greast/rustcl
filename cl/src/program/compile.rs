#[derive(Debug, PartialEq, Eq, Default)]
pub struct Options {}

#[allow(clippy::from_over_into)]
impl Into<Vec<u8>> for &Options {
    fn into(self) -> Vec<u8> {
        Vec::new()
    }
}

use cl3::error_codes::*;
use enum_utils::{ReprFrom, TryFromRepr};
#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    Value = CL_INVALID_VALUE,
}

use crate::device::Device;
use crate::program::Binary;
use std::ffi::{CStr, CString};

use cl3::types::cl_program;
use core::ffi::c_void;
use futures::channel::oneshot::*;

extern "C" fn send_program(program: cl_program, user_data: *mut c_void) {
    unsafe {
        let data = Binary(program);
        let sender = Box::from_raw(user_data as *mut Sender<Binary>);
        sender.send(data).ok().unwrap();
    }
}

use crate::program::Source;

use cl3::program::compile_program;
impl Source {
    pub fn compile(
        self,
        devs: &[Device],
        opt: &Options,
        names: &[&CStr],
        headers: &[Source],
    ) -> Result<Receiver<Binary>, Error> {
        let devs = devs.iter().map(|x| x.0).collect::<Vec<_>>();
        let headers = headers.iter().map(|x| x.0).collect::<Vec<_>>();
        let string = CString::new(opt).unwrap();

        let (send, recv) = channel();
        let send = Box::into_raw(Box::new(send));

        compile_program(
            self.0,
            &devs,
            &string,
            &headers,
            names,
            Some(send_program),
            send as *mut _,
        )
        .map_err(Error::try_from)
        .map_err(Result::unwrap)?;

        Ok(recv)
    }
}
