use cl3::error_codes::*;
use enum_utils::{ReprFrom, TryFromRepr};

#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum CLError {
    Value = CL_INVALID_VALUE,
}

#[derive(Debug, From)]
pub enum Error {
    CL(CLError),
    Type(u64),
}

use super::{Binary, Cast};
use derive_more::*;
#[derive(Deref, Constructor)]
pub struct Executable(pub Binary);

use crate::device::Device;
use cl3::program::*;
impl Cast for Executable {
    type Error = Error;
    fn cast(binary: Binary, dev: &[Device]) -> Result<Self, Self::Error> {
        binary
            .is_type(dev, CL_PROGRAM_BINARY_TYPE_EXECUTABLE)
            .map_err(Error::Type)
            .map(|_| Executable(binary))
    }
}
