pub mod link;
pub use link::*;
pub mod object;
pub use object::*;
pub mod executable;
pub use executable::*;

use cl3::program::create_program_with_binary;
use cl3::types::cl_program;
use derive_more::*;

pub struct Binary(pub cl_program);

pub trait Cast: Sized {
    type Error;
    fn cast(_: Binary, _: &[Device]) -> Result<Self, Self::Error>;
}

impl Cast for Binary {
    type Error = !;
    fn cast(binary: Binary, _: &[Device]) -> Result<Self, Self::Error> {
        Ok(binary)
    }
}

use crate::context::Context;
use crate::device::Device;
impl Context {
    pub fn binary<T, E>(&self, devs: &[Device], srcs: &[&[u8]]) -> Result<T, Error<E>>
    where
        T: Cast<Error = E>,
    {
        let dev_list = devs.iter().map(|x| x.0).collect::<Vec<_>>();
        create_program_with_binary(self.0, &dev_list, srcs)
            .map(Binary)
            .map_err(CLError::try_from)
            .map_err(Result::unwrap)
            .map_err(Error::CLError)
            .and_then(|x| T::cast(x, devs).map_err(Error::Error))
    }
}

use cl3::error_codes::CL_OUT_OF_HOST_MEMORY;
use enum_utils::{ReprFrom, TryFromRepr};
#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum CLError {
    NoHostMemory = CL_OUT_OF_HOST_MEMORY,
}

#[derive(Debug)]
pub enum Error<E> {
    CLError(CLError),
    Error(E),
}

use cl3::program::*;
impl Binary {
    pub fn cast<T, E>(self, devs: &[Device]) -> Result<T, E>
    where
        T: Cast<Error = E>,
    {
        Cast::cast(self, devs)
    }

    pub fn r#type(&self, dev: &Device) -> u32 {
        get_program_build_info(self.0, dev.0, CL_PROGRAM_BINARY_TYPE)
            .unwrap()
            .into()
    }

    pub fn is_type(&self, dev: &[Device], mask: u64) -> Result<(), u64> {
        for dev in dev {
            let actual: u32 = self.r#type(dev);

            let actual = actual as u64;
            if 0 == actual & mask {
                return Err(actual);
            }
        }
        Ok(())
    }

    pub fn source(&self) -> Vec<Vec<u8>> {
        get_program_info(self.0, CL_PROGRAM_BINARIES)
            .unwrap()
            .into()
    }
}
