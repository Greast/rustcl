use super::{Binary, Cast};
use derive_more::*;
#[derive(Deref, DerefMut)]
pub struct Object(pub Binary);

use crate::device::Device;

use cl3::error_codes::*;
use enum_utils::{ReprFrom, TryFromRepr};
#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum CLError {
    Value = CL_INVALID_VALUE,
}

#[derive(Debug, From)]
pub enum Error {
    CLError(CLError),
    Type(u64),
}

use cl3::program::*;

impl Cast for Object {
    type Error = Error;
    fn cast(binary: Binary, dev: &[Device]) -> Result<Self, Self::Error> {
        binary
            .is_type(
                dev,
                CL_PROGRAM_BINARY_TYPE_COMPILED_OBJECT | CL_PROGRAM_BINARY_TYPE_LIBRARY,
            )
            .map_err(Error::Type)
            .map(|_| Object(binary))
    }
}
