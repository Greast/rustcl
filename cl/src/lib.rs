#![feature(never_type)]

pub mod context;
pub mod device;
pub mod kernel;
pub mod memory;
pub mod platform;
pub mod program;
pub mod queue;

#[cfg(test)]
mod test {
    #[test]
    fn test() {
        let t = trybuild::TestCases::new();
        t.pass("builds/**/*.rs");
    }
}
