use cl3::types::cl_platform_id;
pub struct Platform(pub cl_platform_id);

use cl3::platform::{
    get_platform_data, get_platform_ids, CL_PLATFORM_EXTENSIONS, CL_PLATFORM_NAME,
    CL_PLATFORM_PROFILE, CL_PLATFORM_VENDOR, CL_PLATFORM_VERSION,
};
impl Platform {
    pub fn all() -> Result<Vec<Self>, Error> {
        let ids = get_platform_ids()
            .map_err(Error::try_from)
            .map_err(Result::unwrap)?
            .into_iter()
            .map(Platform)
            .collect();
        Ok(ids)
    }

    pub fn platform(&self) -> String {
        let string = get_platform_data(self.0, CL_PLATFORM_PROFILE).unwrap();
        String::from_utf8(string).unwrap()
    }

    pub fn version(&self) -> String {
        let string = get_platform_data(self.0, CL_PLATFORM_VERSION).unwrap();
        String::from_utf8(string).unwrap()
    }

    pub fn name(&self) -> String {
        let string = get_platform_data(self.0, CL_PLATFORM_NAME).unwrap();
        String::from_utf8(string).unwrap()
    }

    pub fn vendor(&self) -> String {
        let string = get_platform_data(self.0, CL_PLATFORM_VENDOR).unwrap();
        String::from_utf8(string).unwrap()
    }

    pub fn extensions(&self) -> String {
        let string = get_platform_data(self.0, CL_PLATFORM_EXTENSIONS).unwrap();
        String::from_utf8(string).unwrap()
    }
}

use cl3::error_codes::CL_OUT_OF_HOST_MEMORY;

use enum_utils::{ReprFrom, TryFromRepr};

#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    NoHostMemory = CL_OUT_OF_HOST_MEMORY,
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn all() {
        Platform::all().unwrap().get(0).unwrap();
    }

    #[test]
    fn platform() {
        Platform::all().unwrap().get(0).unwrap().platform();
    }

    #[test]
    fn version() {
        Platform::all().unwrap().get(0).unwrap().version();
    }

    #[test]
    fn name() {
        Platform::all().unwrap().get(0).unwrap().name();
    }

    #[test]
    fn extensions() {
        Platform::all().unwrap().get(0).unwrap().extensions();
    }

    #[test]
    fn vendor() {
        Platform::all().unwrap().get(0).unwrap().vendor();
    }
}
