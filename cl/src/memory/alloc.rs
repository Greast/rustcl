use super::{Mem, Size};

use crate::context::Context;
pub trait Alloc: Sized {
    type Error;
    fn alloc(self, _: &Context) -> Result<Mem<Self>, Self::Error>;
}

impl Context {
    pub fn mem<T, E>(&self, value: T) -> Result<Mem<T>, E>
    where
        T: Alloc<Error = E>,
    {
        value.alloc(self)
    }
}

use cl3::memory::*;
use core::mem::size_of_val;

use cl3::error_codes::*;
use enum_utils::{ReprFrom, TryFromRepr};
#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    Context = CL_INVALID_CONTEXT,
}

impl<T> Alloc for &[T]
where
    T: Size,
{
    type Error = Error;
    fn alloc(self, ctx: &Context) -> Result<Mem<Self>, Self::Error> {
        let cl = create_buffer(
            ctx.0,
            CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY | CL_MEM_HOST_READ_ONLY,
            size_of_val(&self),
            self.as_ptr() as *mut _,
        )
        .map_err(Error::try_from)
        .map_err(Result::unwrap)?;

        let out = Mem { cl, value: self };

        Ok(out)
    }
}

impl<T> Alloc for &mut [T]
where
    T: Size,
{
    type Error = Error;
    fn alloc(self, ctx: &Context) -> Result<Mem<Self>, Self::Error> {
        let cl = create_buffer(
            ctx.0,
            CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE,
            size_of_val(&self),
            self.as_ptr() as *mut _,
        )
        .map_err(Error::try_from)
        .map_err(Result::unwrap)?;

        let out = Mem { cl, value: self };

        Ok(out)
    }
}
