pub mod alloc;
pub trait Size {
    fn size(&self) -> usize;
}

pub trait Inner: Size {}

macro_rules! impl_size{
    ($($x:ty),*) => {
        $(
            impl Size for $x{
                fn size(&self) -> usize{
                    ::core::mem::size_of_val(&self)
                }
            }
        )*
    }
}

impl_size! {u8, u16, u32, u64, usize, i8, i16, i32, i64, isize, f32, f64}

use cl3::types::cl_mem;
use derive_more::*;
#[derive(Deref)]
pub struct Mem<T> {
    #[deref]
    cl: cl_mem,
    value: T,
}

impl<T> Mem<T> {
    pub fn into(self) -> T {
        self.value
    }
}

use core::mem::size_of_val;

impl<T> Size for Mem<T> {
    fn size(&self) -> usize {
        size_of_val(&self)
    }
}
