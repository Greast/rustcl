use cl3::error_codes::*;

pub trait SetArg<T> {
    type Error;
    fn set(&mut self, _: T) -> Result<(), Self::Error>;
}
use cl3::types::cl_kernel;
use derive_more::*;
pub struct Arg<T> {
    kernel: cl_kernel,
    idx: u32,
    value: T,
}

impl<T> Arg<T>
where
    T: Size,
{
    pub fn new(kernel: cl_kernel, idx: u32, value: T) -> Result<Self, Error> {
        use core::mem::MaybeUninit;
        let mut out = Self {
            kernel,
            idx,
            value: unsafe {
                #[allow(clippy::uninit_assumed_init)]
                MaybeUninit::uninit().assume_init()
            },
        };
        out.set(value)?;
        Ok(out)
    }
}

use crate::memory::Size;
use cl3::kernel::set_kernel_arg;
use enum_utils::{ReprFrom, TryFromRepr};

#[derive(Error, Display, Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum Error {
    Kernel = CL_INVALID_KERNEL,
}

impl<T> SetArg<T> for Arg<T>
where
    T: Size,
{
    type Error = Error;
    fn set(&mut self, value: T) -> Result<(), Self::Error> {
        set_kernel_arg(
            self.kernel,
            self.idx,
            value.size(),
            &value as *const T as *const _,
        )
        .map_err(Error::try_from)
        .map_err(Result::unwrap)?;

        self.value = value;

        Ok(())
    }
}
