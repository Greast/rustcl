use cl3::types::cl_kernel;

pub trait Kernel {
    type Output;
    type Error;
    fn kernel(self, _: cl_kernel) -> Result<Self::Output, Self::Error>;
}

use derive_more::*;

#[derive(Debug, Display, Error)]
pub enum Error {
    #[display(fmt = "Expected {} arguments, got {}", expected, actual)]
    NumArgs { expected: u32, actual: u32 },
    #[display(fmt = "Arg {} got {}", idx, error)]
    Arg {
        idx: u32,
        error: Box<dyn std::error::Error>,
    },
}

impl Kernel for () {
    type Output = ();
    type Error = !;
    fn kernel(self, _: cl_kernel) -> Result<Self::Output, Self::Error> {
        Ok(self)
    }
}
