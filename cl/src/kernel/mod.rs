pub mod arg;
pub mod to;
use cl3::types::cl_kernel;
use derive_more::*;
#[derive(Deref, DerefMut)]
pub struct Kernel<T> {
    pub(crate) kernel: cl_kernel,
    #[deref]
    #[deref_mut]
    value: T,
}

use cl3::error_codes::*;
use enum_utils::{ReprFrom, TryFromRepr};
#[derive(Debug, Eq, PartialEq, TryFromRepr, ReprFrom)]
#[repr(i32)]
pub enum CLError {
    Program = CL_INVALID_PROGRAM,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error<E> {
    CL(CLError),
    Error(E),
}

use crate::program::Executable;
use cl3::kernel::create_kernel;
use std::ffi::CStr;
impl Executable {
    pub fn kernel<T, O, E>(&self, cstr: &CStr, value: T) -> Result<Kernel<O>, Error<E>>
    where
        T: to::Kernel<Output = O, Error = E>,
    {
        create_kernel(self.0 .0, cstr)
            .map_err(CLError::try_from)
            .map_err(Result::unwrap)
            .map_err(Error::CL)
            .and_then(move |kernel| {
                value
                    .kernel(kernel)
                    .map(|value| Kernel { kernel, value })
                    .map_err(Error::Error)
            })
    }
}
