pub mod r#type;
pub use r#type::*;

use cl3::types::*;
pub struct Device(pub cl_device_id);

use cl3::device::*;
impl Device {
    pub fn r#type(&self) -> cl_device_type {
        get_device_info(self.0, CL_DEVICE_TYPE).unwrap().into()
    }
    pub fn vendor_id(&self) -> cl_uint {
        get_device_info(self.0, CL_DEVICE_VENDOR_ID).unwrap().into()
    }
    pub fn max_compute_units(&self) -> cl_uint {
        get_device_info(self.0, CL_DEVICE_MAX_COMPUTE_UNITS)
            .unwrap()
            .into()
    }
    pub fn max_work_items(&self) -> cl_uint {
        get_device_info(self.0, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)
            .unwrap()
            .into()
    }
}
