use cl3::device::*;

use enum_utils::{FromStr, ReprFrom, TryFromRepr};

#[derive(Debug, Eq, PartialEq, FromStr, ReprFrom, TryFromRepr)]
#[repr(u64)]
pub enum Type {
    Cpu = CL_DEVICE_TYPE_CPU,
    Gpu = CL_DEVICE_TYPE_GPU,
    Accelerator = CL_DEVICE_TYPE_ACCELERATOR,
    Custom = CL_DEVICE_TYPE_CUSTOM,
    Default = CL_DEVICE_TYPE_DEFAULT,
    All = CL_DEVICE_TYPE_ALL,
}

use super::Device;
use crate::platform::Platform;
impl Platform {
    pub fn devices(&self, ty: Type) -> Vec<Device> {
        get_device_ids(self.0, ty.into())
            .unwrap()
            .into_iter()
            .map(Device)
            .collect()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use test_case::test_case;
    #[test_case(Type::Cpu)]
    #[test_case(Type::Gpu)]
    #[test_case(Type::Accelerator)]
    #[test_case(Type::Custom)]
    #[test_case(Type::Default)]
    #[test_case(Type::All)]
    fn devices(ty: Type) {
        Platform::all().unwrap().get(0).unwrap().devices(ty);
    }
}
