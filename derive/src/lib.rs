use proc_macro::TokenStream;
use proc_macro_error::*;
use quote::*;
use syn::*;

#[proc_macro_derive(Kernel)]
pub fn kernel(item: TokenStream) -> TokenStream {
    let mut item_struct = parse_macro_input!(item as syn::ItemStruct);

    use Fields::*;
    match item_struct.fields {
        Unnamed(_) => (),
        Named(_) | Unit => abort_call_site!("Struct has to be unnamed"),
    };

    let where_clause = item_struct.generics.make_where_clause();

    for field in &mut item_struct.fields {
        let ty = &field.ty;
        where_clause
            .predicates
            .push(parse_quote!(#ty : ::cl::memory::Size));
        field.ty = parse_quote!(::cl::kernel::arg::Arg<#ty>);
    }

    let mut output = item_struct.clone();

    let ident = item_struct.ident;

    output.ident = format_ident!("{}KernelOutput", output.ident);
    let output_ident = &output.ident;

    let (impl_generics, ty_generics, where_clause) = item_struct.generics.split_for_impl();

    let fields = item_struct.fields;

    let fields_len = fields.len();

    let idx = 0..(fields_len as u32);

    let member = (0..fields_len).map(Index::from);

    let out = quote! {
        #output

        impl #impl_generics ::cl::kernel::to::Kernel for #ident #ty_generics
            #where_clause{
            type Output = #output_ident #ty_generics;
            type Error = ::cl::kernel::to::Error;
            fn kernel(self, cl : ::cl3::types::cl_kernel) -> Result<Self::Output, Self::Error>{
                let actual = ::cl3::kernel::get_kernel_info(cl, ::cl3::kernel::CL_KERNEL_NUM_ARGS).unwrap().into();
                let expected = #fields_len as u32;

                if expected != actual{
                    Err(::cl::kernel::to::Error::NumArgs{
                        expected,
                        actual
                    })
                } else {
                    let out = #output_ident(#(
                        ::cl::kernel::arg::Arg::new(cl, #idx, self.#member).map_err(|error|
                            ::cl::kernel::to::Error::Arg{
                                idx : #idx,
                                error : Box::new(error)
                            })?
                    ),*);

                    Ok(out)
                }
            }
        }


    };

    out.into()
}
